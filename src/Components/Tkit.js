import React from "react";
import { cloneDeep, mapValues } from "lodash";
import { FlowChart, actions } from "@mrblenny/react-flow-chart";

import Sidebar from "./Sidebar";
import Content from "./Holst";
import NodeInnerCustom from "./NodeInnerCustom";
import { chartSimple, nodes } from "../constants";

class Tkit extends React.Component {
  stories = {
    nodes: {},
    links: [],
  };

  constructor(props) {
    super(props);
    this.state = cloneDeep(chartSimple);
  }

  //Обработчик удаления ноды, ссылки
  onDeleteKey = () => {
    const selectedId = this.state.selected.id;
    // Удаляем ноду из списка нод
    delete this.stories.nodes[selectedId];
    // Удаляем ссылку из списка ссылок
    this.stories.links = this.stories.links.filter(
      (link) => link["from"] === selectedId || link["to"] === selectedId
    );
  };

  //Обработчик изменения числа нод
  onNodeSizeChange = (args) => {
    const node = this.state.nodes[args.nodeId];
    if (node) {
      // Добавляем ноду в список
      this.stories.nodes[args.nodeId] = {
        id: args.nodeId,
        // TODO: возможно определить правила формирования объекта
        properties: {
          type: node.properties.type,
          text: node.properties.description,
        },
        type: node.type,
      };
    }
  };

  //Обработчик создания ссылки
  onLinkComplete = (args) => {
    const { fromNodeId, fromPortId, toNodeId, toPortId } = args;
    const formPort = this.state.nodes[fromNodeId].ports[fromPortId];
    const toPort = this.state.nodes[toNodeId].ports[toPortId];
    //Добавляем ссылку в список
    this.stories.links.push({
      from: args.fromPortId,
      to: args.toNodeId,
      port_value:
        formPort.hasOwnProperty("value") ||
        toPort.hasOwnProperty("value") ||
        "null",
    });
  };

  //Обработчик клика по ноде
  onNodeClick = (args) => {
    const node = this.state.nodes[args.nodeId];
    if (node) {
      this.setState({ editableNode: node });
    }
  };

  //Обработчик клика по пустой области
  onCanvasClick = () => {
    this.setState({ editableNode: null });
  };

  render() {
    const chart = this.state;

    const stateActions = mapValues(actions, (func) => (...args) =>
      this.setState(func(...args))
    );

    return (
      <div className="Page">
        <Content>
          <FlowChart
            chart={chart}
            config={{ snapToGrid: true }}
            Components={{ NodeInner: NodeInnerCustom }}
            callbacks={{
              ...stateActions,
              onNodeSizeChange: (args) => {
                this.onNodeSizeChange(args);
                stateActions.onNodeSizeChange(args);
              },
              onLinkComplete: (args) => {
                this.onLinkComplete(args);
                stateActions.onLinkComplete(args);
              },
              onDeleteKey: (args) => {
                this.onDeleteKey(args);
                stateActions.onDeleteKey(args);
              },
              onNodeClick: (args) => {
                this.onNodeClick(args);
                stateActions.onNodeClick(args);
              },
              onCanvasClick: (args) => {
                this.onCanvasClick();
                stateActions.onCanvasClick(args);
              },
            }}
          />
        </Content>
        <Sidebar data={nodes} isNodeEditMode={this.state.editableNode} />
      </div>
    );
  }
}

const App = () => {
  return <Tkit />;
};

export default App;
