import React, { Component } from "react";
import SidebarItem from "./SidebarItem";
import { Select, TextInput } from "./Fields";

class Sidebar extends Component {
  constructor(props) {
    const { data } = props;
    super(props);
    this.nodes = data.nodes;
  }

  handleNodeClick = (node) => {};

  renderFields = () => {
    const { fields } = this.props.isNodeEditMode.properties;
    const fieldsKeys = Object.keys(fields)
    if (fieldsKeys.length === 0) {
      return "No fields to edit";
    }

    return (
      <div className="form">
        {fieldsKeys.map((key) => {
          const field = fields[key];
          const type = field.type;

          const props = {
            ...field,
            title: key,
            key,
            onChange: (e) => {
              // TODO: определить формат сохранения данных
              console.log("value = ", e.target.value);
            },
          };

          if (type === "select") {
            return <Select {...props} />;
          } else if (type === "text") {
            return <TextInput {...props} />;
          } else {
            return `Type is not defined: ${type}`;
          }
        })}
      </div>
    );
  };

  render() {
    const { isNodeEditMode } = this.props;
    return (
      <div className="Sidebar">
        {isNodeEditMode
          ? this.renderFields()
          : this.nodes.map((node, index) => (
              <SidebarItem
                key={index}
                type={node.type}
                ports={node.ports}
                properties={node.properties}
                handleNodeClick={() => this.handleNodeClick(node)}
              />
            ))}
      </div>
    );
  }
}

export default Sidebar;
