import React from "react";

const Node = ({ node, handleClickEvent }) => {
  return (
    <div className="node" onClick={handleClickEvent}>
      {node.properties.title}
    </div>
  );
};

class NodeInnerCustom extends React.Component {
  constructor(props) {
    super(props);
    const { node, config } = props;
    this.node = node;
    this.config = config;
  }

  handleClickEvent = () => {};

  render() {
    switch (this.node.type) {
      case "command":
        return <Node node={this.node} />;
      case "state":
        return (
          <Node node={this.node} handleClickEvent={this.handleClickEvent} />
        );
      case "action":
        return (
          <Node node={this.node} handleClickEvent={this.handleClickEvent} />
        );
      case "condition":
        return (
          <Node node={this.node} handleClickEvent={this.handleClickEvent} />
        );

      default:
        return <div></div>;
    }
  }
}

export default NodeInnerCustom;
