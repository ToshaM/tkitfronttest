import React from "react";

export const Select = ({
  options,
  description,
  is_required,
  title,
  onChange,
}) => {
  const selectOptions = Object.keys(options).map((key) => ({
    title: options[key],
    value: key,
  }));

  return (
    <div className="form-element">
      <label title={description}>
        {title}
        {is_required ? "*" : ""}
      </label>
      <select onChange={onChange}>
        {selectOptions.map(({ value, title }) => (
          <option value={value}>{title}</option>
        ))}
      </select>
    </div>
  );
};

export const TextInput = ({ description, is_required, title, onChange }) => {
  return (
    <div className="form-element">
      <label title={description}>
        {title}
        {is_required ? "*" : ""}
      </label>
      <input onChange={onChange} />
    </div>
  );
};
